(function() {
  'use strict';

  angular
    .module('inspinia').config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('giphy_');
      });

})();
