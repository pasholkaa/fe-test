(function() {
  'use strict';

  angular
    .module('inspinia')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('index', {
        abstract: true,
        url: "/index",
        templateUrl: "app/components/common/content.html"
      })
      .state('index.main', {
        url: "/main",
        templateUrl: "app/main/main.html",
        data: { pageTitle: 'Main page' }
      })
      .state('index.collection', {
        url: "/collection",
        templateUrl: "app/collection/collection.html",
        data: { pageTitle: 'My collection' }
      });

    $urlRouterProvider.otherwise('/index/main');
  }

})();
