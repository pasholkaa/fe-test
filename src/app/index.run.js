(function() {
  'use strict';

  angular
    .module('inspinia')
    .run(runBlock);

  /** @ngInject */
  function runBlock() {
    //$log.debug('runBlock end');
  }

})();
