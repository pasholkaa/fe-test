'use strict';

angular.module('inspinia')
    .controller('CollectionController',  CollectionController);

CollectionController.$inject = ['$scope', '$log', 'localStorageService', '$http'];
function CollectionController ($scope, $log, localStorageService, $http) {
    var vm  = this;

    var list = localStorageService.get('favorite') || '';
    $log.debug(list);

    $http.get("http://api.giphy.com/v1/gifs?api_key=dc6zaTOxFJmzC&ids=" + list).then(function(response) {
        $log.debug(response);
        vm.items = response.data.data;
    }.bind(this));

}
