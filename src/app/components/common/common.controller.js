'use strict';

angular.module('inspinia')
    .factory('Page', function() {
        var title = 'default';
        return {
            title: function() { return title; },
            setTitle: function(newTitle) { title = newTitle }
        };
    })
    .controller('TopNavController',  TopNavController);

TopNavController.$inject = ['$scope','$location' , '$log'];
function TopNavController ($scope, $location, $log) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}
