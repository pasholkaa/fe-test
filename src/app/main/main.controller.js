'use strict';

angular.module('inspinia')
    .factory('Images', function($http, $log) {
        var Images = function () {
            this.apiKey = 'api_key=dc6zaTOxFJmzC';
            this.items = [];
            this.offset = 0;
            this.limit = 40;
            this.busy = false;
            this.action = 'trending';
            this.queryString = '';
            this.itemData = {};
        }

        Images.prototype.makeUrl = function () {
            var url = 'http://api.giphy.com/v1/gifs/';
            var apiKeyStr = this.apiKey;

            switch (this.action) {
                case 'search':
                    url = url + this.action + '?' + apiKeyStr + '&q=' + this.queryString;
                    break;
                default:
                    url = url + this.action + '?' + apiKeyStr
            }

            return url + "&limit=" + this.limit + "&offset=" + this.offset;
        }

        Images.prototype.search = function (query) {
            if (!query) {
                this.items = [];
                this.offset = 0;
                this.action = 'trending';
                this.nextPage();
            }
            this.items = [];
            this.offset = 0;
            this.action = 'search';
            this.queryString = query;
            $log.debug(query);
            this.nextPage();
        }

        Images.prototype.nextPage = function() {
            if (this.busy) return;
            this.busy = true;

            var url = this.makeUrl();
            $log.debug('url:' + url);
            $http.get(url).then(function(response) {
                //$log.debug('----');
                //$log.debug(response);
                var items = response.data.data;
                for (var i = 0; i < items.length; i++) {
                    this.items.push(items[i]);
                    this.offset++;
                    //$log.debug(items[i]);
                    //$log.debug(this.offset);
                }
                //$log.debug(this);
            }.bind(this));
            this.busy = false;
        };

        Images.prototype.getOneById =  function (id, cb) {
            $http.get('http://api.giphy.com/v1/gifs/'+ id + '?' + this.apiKey).then(function(response) {

                //$log.debug(response);
                //this.itemData = response.data.data;
                cb(response.data.data.images.original.url);
                $log.debug(response.data.data.images.original);

            }.bind(this));
            return this;
        }

        return Images;
     })
    .controller('MainController',  MainController);


MainController.$inject = ['$scope', '$log', '$http', '$uibModal','Images', 'localStorageService'];
function MainController($scope, $log, $http, $uibModal, Images, localStorageService) {
    var vm  = this;
    var images  = new Images();

    vm.images   = images;

    vm.search   = function (query) {
        //$log.debug(query)
        images.search(query);
        vm.images = images;
    }

    vm.addToFav = function (id) {
        var list = localStorageService.get('favorite') || '';
        var array = [];

        if (list) {
            var array = list.split(',');
        }

        $log.debug(array);
        $log.debug(id);

        if (!array[id]) {
            array.push(id);

        }
        $log.debug(array);
        localStorageService.set('favorite', array.join(','))
    }

    vm.open = function (id) {
        images.getOneById(id, function (url) {
            $uibModal.open({
                template: '<div class="modal-body">' +
                '<div class="row modal-image-wrapper"><img src="'+ url +'"></div></div>'
            });
        }.bind(this));

    };
}

//ModalInstanceCtrl.$inject = ['$scope', '$uibModalInstance', '$log'];
//function ModalInstanceCtrl ($scope, $uibModalInstance, $log) {
// $log.debug('ModalInstanceCtrl');
//}



