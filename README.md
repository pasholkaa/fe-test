### Install dependencies

```
$ npm install
$ bower install
```

### Build project
```
$ gulp build
```

### Start app
```
$ cd ./dist
```
Run index.html

### File structure.
./src - Source files
./src/main.controller.js - Main controller